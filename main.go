package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"gitlab.com/ashinnv/oddstring"
)

type KratObj struct {
	Uname string
	Upass string
	Ml    string
	Color string
	Breed string
}

func main() {

	go runServer()

	var stopper sync.WaitGroup
	stopper.Add(42)
	stopper.Wait()

}

func runServer() {

	http.HandleFunc("/", genRoot())
	http.HandleFunc("/css", genCss())
	http.HandleFunc("/js", genJs())
	http.HandleFunc("/rec", genRec())

	http.ListenAndServe(":8080", nil)

}

func genRoot() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		srvDat, readErr := os.ReadFile("htm/index.html")
		if readErr != nil {
			fmt.Println("READ ERROR:", readErr)
		}
		w.Header().Set("Content-Type", "text/html")
		w.Write(srvDat)

	}
}

func genCss() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		srvDat, _ := os.ReadFile("htm/css.css")
		w.Header().Set("Content-Type", "text/css")
		w.Write(srvDat)

	}
}

func genJs() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		srvDat, _ := os.ReadFile("htm/js.js")
		w.Header().Set("Content-Type", "text/javascript")
		w.Write(srvDat)

	}
}

func genRec() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		var payload KratObj

		dec := json.NewDecoder(r.Body)
		if decErr := dec.Decode(&payload); decErr != nil {
			fmt.Println("DEC ERR: ", decErr, r.Body)
		}

		fmt.Println("PAYLOAD: ", payload)

		w.Write([]byte("{\"status\":\"Success\"}"))

		if !tryWriteToFile(fmt.Sprintf("%v", payload)) {
			os.Exit(133)
		}

	}
}

func tryWriteToFile(input string) bool {

	if err := os.WriteFile(genName(input)+".json", []byte(input), 0777); err != nil {
		fmt.Println("ERR: ", err)
		return false
	} else {
		return true
	}

}

func _tryWriteToFile(input string) {
	fmt.Println("WTF: ", input)

	if err := os.WriteFile(genName(input)+".json", []byte(input), 0777); err != nil {
		fmt.Println("ERR: ", err)
	}
}

func genName(dat string) string {

	now := strconv.Itoa(int(time.Now().UnixNano()))
	//hash := sha256.Sum256([]byte(dat))
	//hashstr := hash[:]

	mod := oddstring.OddstringModeGenerator(0)

	hashstr := now + "---" + oddstring.RandStringSingle(25, false, mod)

	return now + string(hashstr)

}
