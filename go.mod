module umwelt

go 1.22.2

require gitlab.com/ashinnv/oddstring v0.1.1

replace gitlab.com/ashinnv/oddstring => ./oddstring/
